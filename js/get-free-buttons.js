export default buttons => {
  return [].concat(...buttons).filter(button => button.innerText === '');
}
