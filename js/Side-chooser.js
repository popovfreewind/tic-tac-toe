export default class {
  constructor() {
    this.$sideChooser = document.getElementById('side-chooser');
    this.buttons = this.$sideChooser.getElementsByTagName('button');
    this.eventOptions = {
      detail: {
        side: null,
      },
    };
    this.startEvent = new CustomEvent('startGame', this.eventOptions);
    this.keybordEventHandler = null;
  }

  handleKeyboardEvents(e) {
    const key = e.code;
    if (key === 'ArrowLeft') {
      this.buttons[0].focus();
    } else if (key === 'ArrowRight') {
      this.buttons[1].focus();
    } else if (key === 'Enter') {
      const activeElement = document.activeElement;
      if (activeElement.tagName === 'BUTTON') {
        this.eventOptions.detail.side = activeElement.innerText;
        window.dispatchEvent(this.startEvent);
      }
    }

  };

  init() {
    this.$sideChooser.style.display = 'block';
    this.buttons[0].focus();
    this.keybordEventHandler = this.handleKeyboardEvents.bind(this);
    document.addEventListener('keydown', this.keybordEventHandler);
  }

  destroy() {
    this.$sideChooser.style.display = 'none';
    document.removeEventListener('keydown', this.keybordEventHandler);
  }
}
