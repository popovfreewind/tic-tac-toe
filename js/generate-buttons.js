export default (boardId) => {
  const $board = document.getElementById(boardId);
  const $newBoard = document.createElement('div');
  const rows = 3;
  const cols = 3;
  const buttonArr = [];

  for (let r = 0; r < rows; r++) {
    for (let c = 0; c < cols; c++) {
      const button = document.createElement('button');
      button.setAttribute('data-col', `${c}`);
      button.setAttribute('data-row', `${r}`);
      $newBoard.appendChild(button);
      buttonArr.push(button);
    }
  }
  $board.parentNode.replaceChild($newBoard, $board);
  $newBoard.setAttribute('id', boardId);
  return buttonArr;
}
