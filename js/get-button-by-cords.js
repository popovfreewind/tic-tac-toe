export default (col, row) => {
  return document.querySelector(`[data-col="${col}"][data-row="${row}"]`)
}
