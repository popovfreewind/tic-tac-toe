import generateButtons from './generate-buttons.js';
import getFreeButtons from './get-free-buttons.js';
import selectButtonToMove from './select-button-to-move.js';
import getWinner from './get-the-winner.js';
import getButtonByCords from './get-button-by-cords.js';

export default class {
  constructor() {
    this.$game = document.getElementById('game');
    this.$gameResultText = document.getElementById('game-result-text');
    this.$playerScore = document.getElementById('player-score');
    this.$computerScore = document.getElementById('computer-score');
    this.$restartBtn = document.getElementById('restart-button');
    this.buttons = null;
    this.playerSide = '';
    this.computerSide = '';
    this.restartEvent = new Event('restartGame');
    this.keybordEventHandler = null;
    this.boardBolcked = false;
    this.winner = '';
  }

  handleKeyboardEvents(e) {
    if (this.boardBolcked) return;
    const key = e.code;
    const activeElement = document.activeElement;
    const isRestartBtn = activeElement === this.$restartBtn;
    const isBoardBtn = activeElement.closest('#board');
    if (!isRestartBtn && !isBoardBtn) {
      this.buttons[0].focus();
    }
    let row = isBoardBtn && activeElement.getAttribute('data-row');
    let col = isBoardBtn && activeElement.getAttribute('data-col');
    row = +row;
    col = +col;

    switch (key) {
      case 'ArrowLeft' :
        if (isBoardBtn) {
          if (col > 0) {
            getButtonByCords(col - 1, row).focus();
          }
        }
        break;
      case 'ArrowUp' :
        if (isBoardBtn) {
          if (row > 0) {
            getButtonByCords(col, row - 1).focus();
          }
        } else if (isRestartBtn) {
          this.buttons[this.buttons.length - 2].focus();
        }
        break;
      case 'ArrowRight' :
        if (isBoardBtn) {
          if (col < 2) {
            getButtonByCords(col + 1, row).focus();
          }
        }
        break;
      case 'ArrowDown' :
        if (isBoardBtn) {
          if (row < 2) {
            getButtonByCords(col, row + 1).focus();
          } else if (row === 2) {
            this.$restartBtn.focus();
          }
        }
        break;
      case 'Enter' :
        if (isRestartBtn) {
          window.dispatchEvent(this.restartEvent);
        } else if (isBoardBtn) {
          if (activeElement.innerText) return;
          activeElement.innerText = this.playerSide;
          if (this.getWinner()) {
            this.reset();
          } else {
            this.moveOfComputer();
          }
        }
        break;
    }
  }

  showGameResults() {
    return new Promise(resolve => {
      let resIsVisible = false;
      const maxTimes = 6;
      let counter = 0;
      const animateResult = () => {
        setTimeout(() => {
          if (resIsVisible) {
            this.$gameResultText.style.opacity = '0';
            resIsVisible = false;
          } else {
            this.$gameResultText.style.opacity = '1';
            resIsVisible = true;
          }
          counter++;
          if (counter < maxTimes) {
            animateResult();
          } else {
            resolve();
          }
        }, 500);
      };
      switch (this.winner) {
        case 'computer':
          this.$gameResultText.innerText = 'Computer won!';
          break;
        case 'player':
          this.$gameResultText.innerText = 'You won!';
          break;
        case 'no-winner':
          this.$gameResultText.innerText = 'No winner!';
          break;
        default:
          this.$gameResultText.innerText = '';
      }
      animateResult();
    });
  }

  setScore() {
    if (this.winner === 'player') {
      this.$playerScore.innerText = +this.$playerScore.innerText + 1;
    } else if (this.winner === 'computer') {
      this.$computerScore.innerText = +this.$computerScore.innerText + 1;
    }
  }

  resetScore() {
    this.$playerScore.innerText = '0';
    this.$computerScore.innerText = '0';
  }

  clearBoard() {
    this.buttons.forEach(button => button.innerText = '');
  }

  getWinner() {
    const winnerSide = getWinner(this.buttons);
    if (this.playerSide === winnerSide) {
      this.winner = 'player';
    } else if (this.computerSide === winnerSide) {
      this.winner = 'computer';
    } else if (winnerSide === 'no-winner') {
      this.winner = 'no-winner';
    }
    return this.winner;
  }

  moveOfPlayer() {
    this.boardBolcked = false;
    const buttons = getFreeButtons(this.buttons);
    if (buttons[0]) {
      buttons[0].focus();
    }
  }

  moveOfComputer() {
    this.boardBolcked = true;
    document.activeElement.blur();
    setTimeout(() => {
      const buttons = getFreeButtons(this.buttons);
      if (buttons[0]) {
        const button = selectButtonToMove(buttons);
        button.innerText = this.computerSide;
        if (this.getWinner()) {
          this.reset();
        } else {
          this.moveOfPlayer();
        }
      }
    }, 1000);
  }

  selectFirstPlayer() {
    if (this.playerSide === 'x') {
      this.moveOfPlayer();
    } else if (this.playerSide === 'o') {
      this.moveOfComputer();
    }
  }

  addEventListeners() {
    this.keybordEventHandler = this.handleKeyboardEvents.bind(this);
    document.addEventListener('keydown', this.keybordEventHandler);
  }

  async reset() {
    this.boardBolcked = true;
    await this.showGameResults();
    this.setScore();
    this.winner = '';
    this.clearBoard();
    this.selectFirstPlayer();
  }

  start(side) {
    this.clearBoard();
    this.resetScore();
    this.playerSide = side;
    this.computerSide = side === 'x' ? 'o' : 'x';
    this.$game.style.display = 'block';
    this.selectFirstPlayer();
    this.addEventListeners();
  }

  init() {
    this.buttons = generateButtons('board');
  }

  restart() {
    this.clearBoard();
    this.resetScore();
    this.$game.style.display = 'none';
    document.removeEventListener('keydown', this.keybordEventHandler);
  }
}
