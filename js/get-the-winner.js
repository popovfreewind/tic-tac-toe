import getButtonByCords from './get-button-by-cords.js';

export default buttons => {

  // check by rows and cols
  const checkArray = (arr) => {
    if (!arr.every) arr = Array.from(arr);
    if (arr.every(button => button.innerText === 'x')) return 'x';
    if (arr.every(button => button.innerText === 'o')) return 'o';
  };

  // check by rows
  for (let r = 0; r < 3; r++) {
    const buttons = document.querySelectorAll(`[data-row="${r}"]`);
    const result = checkArray(buttons);
    if (result) return result;
  }

  // check by cols
  for (let c = 0; c < 3; c++) {
    const buttons = document.querySelectorAll(`[data-col="${c}"]`);
    const result = checkArray(buttons);
    if (result) return result;
  }

  // check by diagonals
  const d_1 = [];
  const d_2 = [];

  for (let d = 0; d < 3; d++) {
    d_1.push(getButtonByCords(d, d));
    d_2.push(getButtonByCords(d, 2 - d)); // reverse
  }

  const d_1_result = checkArray(d_1);
  if (d_1_result) return d_1_result;

  const d_2_result = checkArray(d_2);
  if (d_2_result) return d_2_result;

  // if no winner or game not over

  return buttons.every(button => button.innerText) ? 'no-winner' : false;
}
