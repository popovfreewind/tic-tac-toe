import SideChooser from './Side-chooser.js';
import Game from './Game.js';
import showPromo from './show-promo.js';

class App {
  constructor() {
    this.sideChooser = new SideChooser();
    this.game = new Game();
    this.isPromoShowed = false;
  }

  async startGame(e) {
    this.sideChooser.destroy();
    if (!this.isPromoShowed) {
      this.isPromoShowed = true;
      await showPromo();
    }
    this.game.start(e.detail.side);
  }

  chooseSide() {
    this.game.restart();
    this.sideChooser.init();
  }

  initEventListeners() {
    window.addEventListener('startGame', this.startGame.bind(this));
    window.addEventListener('restartGame', this.chooseSide.bind(this));
  }

  init() {
    this.sideChooser.init();
    this.game.init();
    this.initEventListeners();
  }
}

const app = new App();
app.init();
