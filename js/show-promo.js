export default () => new Promise(resolve => {
  const promo = document.getElementById('promo');
  const hidePromo = () => {
    resolve();
    promo.style.display = 'none';
    promo.removeEventListener('ended', hidePromo);
    promo.removeEventListener('error', hidePromo);
  };
  promo.style.display = 'block';
  promo.play();
  promo.addEventListener('ended', hidePromo);
  promo.addEventListener('error', hidePromo);
});
